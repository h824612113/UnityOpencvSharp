﻿using OpenCvSharp;
using System;
using UnityEngine;

public class ffmpeg : MonoBehaviour
{
    VideoCapture capture;
    bool isWrite = true;
    VideoWriter writer;
    Mat image = new Mat();
    public Texture2D rt;
    byte[] texture;
    float fireRate = 0.04F;
    private float nextFire = 0.0F;
    static int mPreviewWidth = 640;
    static int mPreviewHeight = 480;
    UnityEngine.Rect rect = new UnityEngine.Rect(0, 0, mPreviewWidth, mPreviewHeight);
    // Use this for initialization
    void Start()
    {
        rt = new Texture2D(mPreviewWidth, mPreviewHeight, TextureFormat.RGB565, false);

        // Opens MP4 file (ffmpeg is probably needed)
        capture = new VideoCapture(0);
        //capture = new VideoCapture(Application.streamingAssetsPath + "/1.mp4");

        //fireRate = (float)(1000f / capture.Fps)/1000;
        
        try
        {
            writer = new VideoWriter();
            writer.Open(Application.streamingAssetsPath + "/test.avi", FourCC.MJPG, 25, new Size(mPreviewWidth, mPreviewHeight), true);
        }
        catch(Exception e)
        {
            Debug.Log(e.ToString());
        }

        Debug.Log(capture.Fps);
        Debug.Log(writer.FileName);
    }


    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnGUI()
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            capture.Read(image);
            //Debug.Log(image.Width + "  " + image.Height);
            texture = image.ToBytes(".png");

            rt.LoadImage(texture);
            rt.Apply(); 
            if (isWrite&& writer.IsOpened())
            {
                //Debug.Log("write");
                writer.Write(image);
            }
        }
        if (rt != null)
        {
            GUI.DrawTexture(rect, rt, ScaleMode.StretchToFill);
        }

        if(GUI.Button(new UnityEngine.Rect(0,Screen.height-30,100,30),"Stop"))
        {
            isWrite = false;
            writer.Release();
        }

    }
}
